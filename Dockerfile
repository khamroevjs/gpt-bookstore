FROM amazoncorretto:17-alpine

# Set the working directory
WORKDIR /app

# Copy the JAR file into the container at /app
COPY build/libs/bookstore-1.0.jar /app

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Run the JAR file
CMD ["java", "-jar", "bookstore-1.0.jar"]
