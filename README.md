## How to run the Bookstore Application

**Prerequisites**:
- Gradle 8.x
- Docker
- JDK 17

**Step 1: Ensure Prerequisites**  
Before running the application, make sure that you have
Docker and JDK 17 installed on your system.

**Step 2: Start Docker Engine**  
Ensure that the Docker engine is running.
You can usually start Docker through the Docker Desktop application
on Windows or through the command line on Linux/Mac.

**Step 3: Deploy the Application**

**On Windows**:
- Open a command prompt.
- Navigate to the root directory of the project where your `deploy.bat` script is located.
- Execute the following command:
  ```shell
  .\deploy.bat
  ```
  This script will build the project and deploy it along with a PostgreSQL database container.

**On Linux/MacOS**:
- Open a terminal.
- Navigate to the root directory of the project where your `deploy.sh` script is located.
- Execute the following command:
  ```shell
  chmod +x deploy.sh  # Make the script executable (if not already)
  ./deploy.sh
  ```
  This script will build the project and deploy it along with a PostgreSQL database container.

**Step 4: Access Swagger Documentation**  
Once the application is running, you can access the Swagger documentation for the API by opening a web browser and navigating to:
```
http://localhost:8080/docs
```
This Swagger documentation provides details on the API endpoints and how to interact with the Todo List application.

**Step 5: Interact with the Application**  
You can now use the Swagger UI to test the API and interact with the Todo List application. Create, read, update, and delete todo items as needed.

**Step 6: Stop the Application (Optional)**  
To stop the application, open a terminal or command prompt, navigate to the root directory of the project, and execute the following command:
```shell
docker-compose stop
```


## Feedback

**Was it easy to complete the task using AI?**
- It was relatively easy. Although I faced some challenges, this technology definitely saves a significant amount of time.

**How long did the task take you to complete?**
- About 5 hours.

**Was the code ready to run after generation? What did you have to change to make it usable?**
- Not always. Usually, I needed to change names of classes/methods/variables. Also, often I needed to refer to documentations of libraries to use the latest APIs.

**Which challenges did you face during completion of the task?**
- The main problem was familiarizing the ChatGPT with the context of the task. ChatGPT usually doesn't have the full picture of a task, and that's why it may not provide relevant solutions. It would be great if there was an IntelliJ plugin that could "see" the project entirely and suggest relevant solutions.

**Which specific prompts did you learn as a good practice to complete the task?**
- It's hard to choose one.

## References
ChatGPT Conversation logs also can be viewed at the following link:  
https://chat.openai.com/share/68d400ec-8cf5-456f-afae-b4d6819bba36
