@echo off
REM Step 1: Build the project using Gradle
echo Step 1: Building the project using Gradle

call gradle bootJar

REM Check if the build was successful
if %ERRORLEVEL% equ 0 (
  echo Build successful
) else (
  echo Build failed. Exiting.
  exit /b 1
)

REM Step 2: Deploy the project using Docker Compose
echo Step 2: Deploying the project using Docker Compose
docker-compose up -d

REM Check if the deployment was successful
if %ERRORLEVEL% equ 0 (
  echo Deployment successful
) else (
  echo Deployment failed. Exiting.
  exit /b 1
)

echo Project deployment is complete.
