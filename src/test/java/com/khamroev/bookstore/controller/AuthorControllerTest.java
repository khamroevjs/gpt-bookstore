package com.khamroev.bookstore.controller;

import com.khamroev.bookstore.model.Author;
import com.khamroev.bookstore.repository.AuthorRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthorController.class)
public class AuthorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthorRepository authorRepository;

    @Test
    public void testCreateAuthor() throws Exception {
        // Given
        long id = 1L;
        Author author = new Author("Author");
        author.setId(id);
        // When
        when(authorRepository.save(any())).thenReturn(author);
        String authorJson = String.format("{\"name\": \"%s\"}", author.getName());
        // Then
        mockMvc.perform(post("/api/authors")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(authorJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").value(author.getId()))
                .andExpect(jsonPath("name").value(author.getName()));
    }


    @Test
    public void testGetAuthorById() throws Exception {
        // Given
        long id = 1L;
        Author author = new Author("Author");
        author.setId(id);
        // When
        when(authorRepository.findById(any())).thenReturn(Optional.of(author));
        // Then
        mockMvc.perform(get("/api/authors/" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(author.getId()))
                .andExpect(jsonPath("name").value(author.getName()));
    }

    @Test
    public void testGetAuthorById_notFound() throws Exception {
        // When
        when(authorRepository.findById(any())).thenReturn(Optional.empty());
        // Then
        mockMvc.perform(get("/api/authors/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateAuthor() throws Exception {
        // Given
        long id = 1L;
        String newName = "New-Author";
        Author author = new Author("Old-Author");
        author.setId(id);

        // When
        when(authorRepository.findById(id)).thenReturn(Optional.of(author));
        author.setName(newName);
        when(authorRepository.save(author)).thenReturn(author);

        // Then
        String authorJson = String.format("{\"id\": %d, \"name\": \"%s\"}", id, newName);
        mockMvc.perform(put("/api/authors/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(authorJson))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateAuthor_notFound() throws Exception {
        // When
        when(authorRepository.findById(any())).thenReturn(Optional.empty());

        // Then
        String authorJson = String.format("{\"id\": %d, \"name\": \"%s\"}", 1, "Author");
        mockMvc.perform(put("/api/authors/1")
                        .contentType(MediaType.APPLICATION_JSON).
                        content(authorJson))
                .andExpect(status().isNotFound());
        verify(authorRepository, never()).save(any());
    }

    @Test
    public void testDeleteAuthor() throws Exception {
        // Given
        long id = 1L;

        // When
        when(authorRepository.existsById(id)).thenReturn(true);

        // Then
        mockMvc.perform(delete("/api/authors/" + id))
                .andExpect(status().isNoContent());
        verify(authorRepository).deleteById(id);
    }

    @Test
    public void testDeleteAuthor_notFound() throws Exception {
        // Given
        long id = 1L;

        // When
        when(authorRepository.existsById(id)).thenReturn(false);

        // Then
        mockMvc.perform(delete("/api/authors/" + id))
                .andExpect(status().isNotFound());
        verify(authorRepository, never()).deleteById(id);
    }


    @Test
    public void testSearchAuthorsByNameContaining() throws Exception {
        // Given
        Author author1 = new Author("Author");
        Author author2 = new Author("ABC-Author-ABC");
        // When
        when(authorRepository.findByNameContaining("Author")).thenReturn(List.of(author1, author2));
        // Then
        mockMvc.perform(get("/api/authors/search/name/Author"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void testSearchAuthorsByNameContaining_notFound() throws Exception {
        // Given
        String name = "Author";
        // When
        when(authorRepository.findByNameContaining(name)).thenReturn(List.of());
        // Then
        mockMvc.perform(get("/api/authors/search/name/" + name))
                .andExpect(status().isNotFound());
    }
}
