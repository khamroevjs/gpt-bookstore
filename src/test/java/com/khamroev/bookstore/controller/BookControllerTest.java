package com.khamroev.bookstore.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.khamroev.bookstore.model.Author;
import com.khamroev.bookstore.model.Book;
import com.khamroev.bookstore.model.Genre;
import com.khamroev.bookstore.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

@WebMvcTest(BookController.class)
public class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookRepository bookRepository;

    @Test
    public void testCreateBook() throws Exception {
        // Given
        Book book = new Book(1L, "Sample Book", null, null, 100, 100);
        when(bookRepository.save(any())).thenReturn(book);
        // Then
        String bookJson = """
                {
                    "title": "Sample Book",
                    "price": 100,
                    "quantity": 100
                }
                """;
        mockMvc.perform(post("/api/books")
                        .contentType("application/json")
                        .content(bookJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").value(book.getId()))
                .andExpect(jsonPath("title").value(book.getTitle()))
                .andExpect(jsonPath("price").value(book.getPrice()))
                .andExpect(jsonPath("quantity").value(book.getQuantity()));
    }

    @Test
    public void testGetBookById() throws Exception {
        // Given
        Book book = new Book(1L, "Sample Book", null, null, 100, 100);
        // When
        when(bookRepository.findById(any())).thenReturn(Optional.of(book));
        // Then
        mockMvc.perform(get("/api/books/" + book.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("title").value("Sample Book"))
                .andExpect(jsonPath("price").value(100))
                .andExpect(jsonPath("quantity").value(100));
    }

    @Test
    public void testGetBookById_notFound() throws Exception {
        //When
        when(bookRepository.findById(any())).thenReturn(Optional.empty());
        // Then
        mockMvc.perform(get("/api/books/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateBook() throws Exception {
        Book book = new Book(1L, "Old Book", null, null, 100, 100);

        when(bookRepository.findById(any())).thenReturn(Optional.of(book));
        book.setTitle("New Book");
        book.setPrice(1000);
        book.setQuantity(1000);
        when(bookRepository.save(any())).thenReturn(book);

        String bookJson = """
                {
                    "title": "New Book",
                    "price": 1000,
                    "quantity": 1000
                }
                """;
        mockMvc.perform(put("/api/books/" + book.getId())
                        .contentType("application/json")
                        .content(bookJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title").value("New Book"))
                .andExpect(jsonPath("price").value(1000))
                .andExpect(jsonPath("quantity").value(1000));
    }

    @Test
    public void testUpdateBook_notFound() throws Exception {
        when(bookRepository.findById(any())).thenReturn(Optional.empty());
        String bookJson = """
                {
                    "title": "New Book",
                    "price": 1000,
                    "quantity": 1000
                }
                """;
        mockMvc.perform(put("/api/books/1")
                        .contentType("application/json")
                        .content(bookJson))
                .andExpect(status().isNotFound());

        verify(bookRepository, never()).save(any());
    }

    @Test
    public void testDeleteBook() throws Exception {
        when(bookRepository.existsById(any())).thenReturn(true);
        mockMvc.perform(delete("/api/books/1"))
                .andExpect(status().isNoContent());
        verify(bookRepository).deleteById(any());
    }

    @Test
    public void testDeleteBook_notFound() throws Exception {
        when(bookRepository.existsById(any())).thenReturn(false);
        mockMvc.perform(delete("/api/books/1"))
                .andExpect(status().isNotFound());
        verify(bookRepository, never()).deleteById(any());
    }


    @Test
    public void testSearchBooksByTitle() throws Exception {
        // Given
        Book book1 = new Book("Sample 1", null, null, 100, 100);
        Book book2 = new Book("2 Sample 2", null, null, 10, 10);
        // When
        when(bookRepository.findByTitleContaining("Sample")).thenReturn(List.of(book1, book2));
        // Then
        mockMvc.perform(get("/api/books/search/title/Sample"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void testSearchBooksByTitle_emptyList() throws Exception {
        when(bookRepository.findByTitleContaining(any())).thenReturn(List.of());
        mockMvc.perform(get("/api/books/search/title/Sample"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void testSearchBookByAuthor() throws Exception {
        // Given
        Book book1 = new Book("Sample 1", new Author("Author 1"), null, 100, 100);
        Book book2 = new Book("2 Sample 2", new Author("Author 1"), null, 10, 10);
        // When
        when(bookRepository.findByAuthor_NameContaining("Author 1")).thenReturn(List.of(book1, book2));
        // Then
        mockMvc.perform(get("/api/books/search/author/Author 1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void testSearchBooksByGenre() throws Exception {
        // Given
        Book book1 = new Book("Sample 1", null, new Genre("Detective"), 100, 100);
        Book book2 = new Book("2 Sample 2", null, new Genre("Detective"), 10, 10);
        // When
        when(bookRepository.findByGenre_NameContaining("Detective")).thenReturn(List.of(book1, book2));
        // Then
        mockMvc.perform(get("/api/books/search/genre/Detective"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }
}


