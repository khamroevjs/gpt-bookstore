package com.khamroev.bookstore.controller;

import com.khamroev.bookstore.model.Genre;
import com.khamroev.bookstore.repository.GenreRepository;
import org.aspectj.weaver.GeneratedReferenceTypeDelegate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GenreController.class)
public class GenreControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GenreRepository genreRepository;

    @Test
    public void testCreateGenre() throws Exception {
        Genre genre = new Genre(1L, "Detective");
        // Set up mock data and behavior using Mockito for genreRepository
        when(genreRepository.save(any())).thenReturn(genre);
        String genreJson = "{\"name\":\"Detective\"}";
        mockMvc.perform(post("/api/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(genreJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").value("1"))
                .andExpect(jsonPath("name").value("Detective"));
    }

    @Test
    public void testGetGenreById() throws Exception {
        Genre genre = new Genre(1L, "Detective");

        when(genreRepository.findById(any())).thenReturn(Optional.of(genre));

        mockMvc.perform(get("/api/genres/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value("1"))
                .andExpect(jsonPath("name").value("Detective"));
    }

    @Test
    public void testGetGenreById_notFound() throws Exception {
        when(genreRepository.findById(any())).thenReturn(Optional.empty());
        mockMvc.perform(get("/api/genres/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateGenre() throws Exception {
        // Given
        Genre genre = new Genre(1L, "Detective");
        // When
        when(genreRepository.findById(any())).thenReturn(Optional.of(genre));
        genre.setName("New Detective");
        when(genreRepository.save(any())).thenReturn(genre);
        // Then
        String genreJson = "{\"id\":1,\"name\":\"New Detective\"}";
        mockMvc.perform(put("/api/genres/" + genre.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(genreJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value("1"))
                .andExpect(jsonPath("name").value("New Detective"));
    }

    @Test
    public void testUpdateGenre_notFound() throws Exception {
        // When
        when(genreRepository.findById(any())).thenReturn(Optional.empty());
        // Then
        String genreJson = "{\"id\":1,\"name\":\"New Detective\"}";
        mockMvc.perform(put("/api/genres/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(genreJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteGenre() throws Exception {
        when(genreRepository.existsById(any())).thenReturn(true);
        mockMvc.perform(delete("/api/genres/1"))
                .andExpect(status().isNoContent());

        verify(genreRepository).deleteById(any());
    }

    @Test void testDeleteGenre_notFound() throws Exception {
        when(genreRepository.findById(any())).thenReturn(Optional.empty());
        mockMvc.perform(delete("/api/genres/1"))
                .andExpect(status().isNotFound());
        verify(genreRepository, never()).deleteById(any());
    }

    @Test
    public void testSearchGenresByNameContaining() throws Exception {
        // Given
        Genre genre1 = new Genre("Science Fiction");
        Genre genre2 = new Genre("Fiction");
        // When
        when(genreRepository.findByNameContaining("Fic")).thenReturn(List.of(genre1, genre2));
        // Then
        mockMvc.perform(get("/api/genres/search/name/Fic"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void testSearchGenresByNameContaining_notFound() throws Exception {
        // When
        when(genreRepository.findByNameContaining("Fic")).thenReturn(List.of());
        // Then
        mockMvc.perform(get("/api/genres/search/name/Fic"))
                .andExpect(status().isNotFound());
    }
}
