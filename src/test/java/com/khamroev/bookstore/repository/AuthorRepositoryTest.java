package com.khamroev.bookstore.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import com.khamroev.bookstore.model.Author;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class AuthorRepositoryTest {
    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void testFindAuthorsByNameContaining() {
        Author author1 =  new Author("Author");
        Author author2 =  new Author("ABC-Author-ABC");
        Author author3 =  new Author("Name");
        authorRepository.saveAll(List.of(author1, author2, author3));

        List<Author> authors = authorRepository.findByNameContaining("Author");

        assertEquals(2, authors.size());
    }
}

