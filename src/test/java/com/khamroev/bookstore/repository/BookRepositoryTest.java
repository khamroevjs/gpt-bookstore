package com.khamroev.bookstore.repository;

import com.khamroev.bookstore.model.Author;
import com.khamroev.bookstore.model.Book;
import com.khamroev.bookstore.model.Genre;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class BookRepositoryTest {
    @Autowired
    private BookRepository bookRepository;

    @Test
    public void testFindBooksByTitleContaining() {
        Book book1 = new Book("Sample 1", null, null, 100, 100);
        Book book2 = new Book("2 Sample 2", null, null, 10, 10);
        Book book3 = new Book("Title", null, null, 10, 10);
        bookRepository.saveAll(List.of(book1, book2, book3));

        List<Book> books = bookRepository.findByTitleContaining("Sample");

        assertEquals(2, books.size());
    }

    @Test
    public void testFindBooksByAuthorNameContaining() {
        Author author = new Author("AuthorName");
        Book book1 = new Book("BookTitle1", author, null, 100, 100);
        Book book2 = new Book("BookTitle2", author, null, 100, 100);
        Book book3 = new Book("BookTitle3", author, null, 100, 100);
        Book book4 = new Book("BookTitle3", null, null, 100, 100);
        bookRepository.saveAll(List.of(book1, book2, book3, book4));

        List<Book> books = bookRepository.findByAuthor_NameContaining("AuthorName");

        assertEquals(3, books.size());
    }

    @Test
    public void testFindBooksByGenreNameContaining() {
        Genre genre = new Genre("Science Fiction");
        Book book1 = new Book("BookTitle1", null, genre, 100, 100);
        Book book2 = new Book("BookTitle2", null, genre, 100, 100);
        Book book3 = new Book("BookTitle3", null, null, 100, 100);
        bookRepository.saveAll(List.of(book1, book2, book3));

        List<Book> books = bookRepository.findByGenre_NameContaining("Fiction");

        assertEquals(2, books.size());
    }
}
