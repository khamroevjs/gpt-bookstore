package com.khamroev.bookstore.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import com.khamroev.bookstore.model.Genre;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class GenreRepositoryTest {
    @Autowired
    private GenreRepository genreRepository;

    @Test
    public void testFindGenresByNameContaining() {
        Genre genre1 = new Genre("Science Fiction");
        Genre genre2 = new Genre("Fiction");
        Genre genre3 = new Genre("Detective");
        genreRepository.saveAll(List.of(genre1, genre2, genre3));

        List<Genre> genres = genreRepository.findByNameContaining("Fic");

        assertEquals(2, genres.size());
    }
}
